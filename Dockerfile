FROM ubuntu
RUN apt-get update
RUN apt-get install -y curl git
RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
RUN chmod +x /usr/local/bin/gitlab-runner
RUN gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runne
RUN useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
RUN gitlab-runner start